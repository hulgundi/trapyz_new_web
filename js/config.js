


$(window).bind("load",function(){



          //Same Height for divs
          //home-page
          var x = ".thumbnail-home";

          function eqHeight(x){
            var heights = $(x).map(function() {
                  return $(this).height();
              }).get(),

              maxHeight = Math.max.apply(null, heights);

              $(x).height(maxHeight);
          }

          eqHeight(x);
          x = ".thumbnails-trapyz";
          eqHeight(x);
          x = ".thumbnail-pricing";
          eqHeight(x);
          x= ".pri-thumbnail";
          eqHeight(x);
          x= ".contact-card";
          eqHeight(x);
          x= ".thumbnail-solutions";
          eqHeight(x);
          x=".thumbnail-sdk"
          eqHeight(x);
		    	window.sr = ScrollReveal();
				sr.reveal('.page-head' , { duration: 400 });
				sr.reveal('.wow' , 300);
				sr.reveal('.wow-phone' , { origin: 'bottom', distance:'300px', duration: 1000,
				delay: 300, opacity:0, scale:1, easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)'});


				//Navbar Config
				/**
				 * Listen to scroll to change header opacity class
				 */
				function checkScroll(){
				    var startY = $('.navbar').height(); //The point where the navbar changes in px

				    if($(window).scrollTop() > startY){
				        $('.navbar').addClass("scrolled");
				        $('.navbar-default .navbar-nav>li>a').addClass("navbar-dark");
				        $('.navbar-default .navbar-nav>li>a').addClass("button-dark");
				        $('.navbar-default .navbar-nav>li:last-child').css("button-dark");

				        $('.svg').attr('src', 'assets/logo-brand.svg');

				    }else{
				        $('.navbar').removeClass("scrolled");
				        $('.navbar-default .navbar-nav>li>a').removeClass("navbar-dark");
				        $('.navbar-default .navbar-nav>li>a').removeClass("button-dark");
				        $('.navbar-default .navbar-nav>li:last-child').removeClass("button-dark");
				        $('.svg').attr('src', 'assets/logo-white.svg');
				    }
				}

				if($('.navbar').length > 0){
				    $(window).on("scroll load resize", function(){
				        checkScroll();
				    });
				}

				


	});
